### 项目介绍
+ 校友捐赠管理系统(Alumni donation management system)主要用途是用于燕山大学基金会的捐赠流程管理。
+ 该仓库是项目[前端地址](https://gitee.com/wanxiaoguo/adms)
+ 项目的[微服务后端地址](https://gitee.com/liuyongtest/xiaoyou)
+ 项目的[单体程序Springboot后端地址]()

### 项目后端搭建
+ 新建springboot项目，在pom.xml添加相关依赖
+ 根据需要使用 `utils/CodeGenerator` 生成代码 
+ 需改项目配置文件 `application.yml`
+ `utils` 工具类
  + `Constants` 定义常量的类 
  + `Result` 返回json数据的格式 （data, status , msg) 数据 状态码 消息类型 (未用)
  + `ResultGenerator` : 封装一些 返回Result方法 （未用)
  + `CodeGenerator`: `mybatisplus`的代码生成器,包含自定义controller层代码输出
  + `InitUtil`: 用来初始化参数信息包括 **分页默认值、like判断**
  + `JwtUtill`: 利用Jwt生成token 
  + `CommonResult`: 使用这个类 返回json数据的格式
  + `ResultCode`: 自定义返回状态码和消息

+ `config` 配置类
  + `DefaultFastjsonConfig` : fastjson配置

+ 启动类
  + 需要加上`@MapperScan`注解

### 登录信息保存
+ 导入JWT依赖
```xml
		<dependency>
			<groupId>com.auth0</groupId>
			<artifactId>java-jwt</artifactId>
			<version>3.10.3</version>
		</dependency>
```

### mybatis-plus的使用 
+ 对于实体类的字段，mybatis-plus默认会认为数据库的字段，在字段对应不上的报错
  + `@TableField(exist = false)` 表明不需要在数据库对应



### 问题

+ `java.sql.SQLFeatureNotSupportedException`
  + 这里的问题是mybatis和druid的版本不一致 mybatis 3.5.4 和 druid 1.1.12 
  + 把druid版本更新到1.1.23 问题解决

+ Java8 `LocalDateTime`和`Date`相互转换
```java
    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }
    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
```

+ fastjson
  + FastJson序列化时过滤字段使用注解`@JSONField(serialize = false)`
