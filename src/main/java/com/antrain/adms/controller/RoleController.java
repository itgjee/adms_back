package com.antrain.adms.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSONObject;
import com.antrain.adms.pojo.Role;
import com.antrain.adms.service.IRoleService;
import com.antrain.adms.utils.CommonResult;
import com.antrain.adms.utils.InitUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
*/
@RestController
@RequestMapping("/roles")
public class RoleController {
    private final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Resource
    private IRoleService iRoleService;

    /**
    * 进行分页查询
    */
    @GetMapping
    public CommonResult getList(@RequestParam Map<String, Object> param) {
        InitUtil.initPage(param);
        int num = Integer.parseInt(param.get("page").toString());
        int limit = Integer.parseInt(param.get("limit").toString());
        QueryWrapper<Role> wrapper = new QueryWrapper<>();
        InitUtil.initEq(param, wrapper, "active");
        InitUtil.initLike(param, wrapper, "name");
        IPage<Role> page = new Page<>(num, limit);
        return CommonResult.success(iRoleService.page(page, wrapper));
    }

    /**
     * 通过主键id在表中查找信息
    */
    @GetMapping("/{id}")
    public CommonResult getById(@PathVariable int id) {
        Role iRole = iRoleService.getById(id);
        if (iRole == null) {
            return CommonResult.failed();
        }
        return CommonResult.success(iRole);
    }

    /**
     * 获取所有有效的角色
    */
    @GetMapping("/all")
    public CommonResult getAll() {
        QueryWrapper<Role> wrapper = new QueryWrapper<>();
        wrapper.eq("active", 1);
        JSONObject jsonObject = new JSONObject();
        List<Role> list = iRoleService.list(wrapper);
        for (Role i : list) {
          jsonObject.put(i.getId().toString(), Map.of("name",i.getName()));
        }
        return CommonResult.success(jsonObject);
    }

    /**
    * name : 需要检查那个字段的value是否在数据库存在
    * 比如/check/RegistworkName?value=阿司匹林  ：检查数据库是否存在RegistworkName='阿司匹林'
    */
    @GetMapping("/check/{name}")
    public CommonResult checkName(@PathVariable String name,@RequestParam String value){
        QueryWrapper<Role> wrapper = new QueryWrapper<>();
        wrapper.eq(name, value);
        if (iRoleService.getOne(wrapper) != null){
            return CommonResult.failed();
        }
        return CommonResult.success();
    }

    /**
     * 向表中添加一条记录
    */
    @PostMapping()
    public CommonResult save(@RequestBody Role iRole) {
        if (iRoleService.save(iRole)){
            return CommonResult.successMsg("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    /**
     * 根据表中的主键修改 根据表中字段相应的修改，如果存在则修改
    */
    @PutMapping("/{id}")
    public CommonResult update(@RequestBody Role iRole, @PathVariable int id) {
        iRole.setId(id);
        if (iRoleService.updateById(iRole)){
            return CommonResult.successMsg("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

    /**
     * active 字段 相当于激活一样 设置字段的有效性 active=1 有效
    */
    @PutMapping("/back/{id}")
    public CommonResult update(@PathVariable int id) {
        Role iRole = new Role();
        iRole.setId(id);
        iRole.setActive(1);
        if (iRoleService.updateById(iRole)){
            return CommonResult.successMsg("激活成功");
        }
        return CommonResult.failed("激活失败");
    }

    /**
     * active 字段 相当于激活一样 设置字段的有效性 active=0 无效 逻辑删除
    */
    @PutMapping("/del/{id}")
    public CommonResult LoginDelById(@PathVariable int id) {
        Role iRole = new Role();
        iRole.setId(id);
        iRole.setActive(0);
        if (iRoleService.updateById(iRole)){
            return CommonResult.successMsg("设置失效成功");
        }
        return CommonResult.failed("设置失效失败");
    }

    /**
     * 批量设置 active=0
    */
    @PutMapping("/batchDel")
    public CommonResult LoginBatchDel(@RequestParam String ids) {
        Role iRole = new Role();
        iRole.setActive(0);
        String[] idList = ids.split(",");
        for (String id : idList) {
            iRole.setId(Integer.parseInt(id));
            iRoleService.updateById(iRole);
        }
        return CommonResult.successMsg("批量失效成功");
    }

    /**
     * 在数据表中真正的删除一条记录
    */
    @DeleteMapping("/{id}")
    public CommonResult delById(@PathVariable int id) {
        if (id<6) {
            return CommonResult.failed("不能删除系统的预留项");
        }
        logger.debug("正在删除id: "+id);
        if (iRoleService.removeById(id)){
            return CommonResult.successMsg("删除成功");
        }
        return CommonResult.failed("删除失败");
    }

}
