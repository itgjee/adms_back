package com.antrain.adms.controller;

import java.util.Map;
import javax.annotation.Resource;

import com.antrain.adms.service.IUserService;
import com.antrain.adms.pojo.User;

import com.antrain.adms.utils.CommonResult;
import com.antrain.adms.utils.InitUtil;
import com.antrain.adms.utils.ShaUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
*/
@RestController
@RequestMapping("/users")
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private IUserService iUserService;

    
    /**
     * 进行分页查询
    */
    @GetMapping
    public CommonResult getList(@RequestParam Map<String, Object> param) {
        InitUtil.initPage(param);
        int num = Integer.parseInt(param.get("page").toString());
        int limit = Integer.parseInt(param.get("limit").toString());
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        InitUtil.initEq(param, wrapper, "active");
        Object name = param.get("name");
        if (!StringUtils.isEmpty(name)){
            wrapper.like("username", name).or().like("realname", name);
        }
        IPage<User> page = new Page<>(num, limit);
        return CommonResult.success(iUserService.page(page, wrapper));
    }

    /**
     * 通过主键id在表中查找信息
    */
    @GetMapping("/{id}")
    public CommonResult getById(@PathVariable int id) {
        User iUser = iUserService.getById(id);
        if (iUser == null) {
            return CommonResult.failed();
        }
        return CommonResult.success(iUser);
    }

    /**
    * name : 需要检查那个字段的value是否在数据库存在
    * 比如/check/RegistworkName?value=阿司匹林  ：检查数据库是否存在RegistworkName='阿司匹林'
    */
    @GetMapping("/check/{name}")
    public CommonResult checkName(@PathVariable String name,@RequestParam String value){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq(name, value);
        if (iUserService.getOne(wrapper) != null){
            return CommonResult.failed();
        }
        return CommonResult.success();
    }

    /**
     * 向表中添加一条记录
    */
    @PostMapping()
    public CommonResult save(@RequestBody User iUser) {
        if (StringUtils.isEmpty(iUser.getPassword())){
            iUser.setPassword(ShaUtil.getSHA256("123456"));
        } else {
            iUser.setPassword(ShaUtil.getSHA256(iUser.getPassword()));
        }
        if (iUserService.save(iUser)){
            return CommonResult.successMsg("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    /**
     * 根据表中的主键修改 根据表中字段相应的修改，如果存在则修改
    */
    @PutMapping("/{id}")
    public CommonResult update(@RequestBody User iUser, @PathVariable int id) {
        iUser.setId(id);
        if (!StringUtils.isEmpty(iUser.getPassword())){
            iUser.setPassword(ShaUtil.getSHA256(iUser.getPassword()));
        }
        if (iUserService.updateById(iUser)){
            return CommonResult.successMsg("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

    /**
     * active 字段 相当于激活一样 设置字段的有效性 active=1 有效
    */
    @PutMapping("/back/{id}")
    public CommonResult update(@PathVariable int id) {
        User iUser = new User();
        iUser.setId(id);
        iUser.setActive(1);
        if (iUserService.updateById(iUser)){
            return CommonResult.successMsg("激活成功");
        }
        return CommonResult.failed("激活失败");
    }

    /**
     * active 字段 相当于激活一样 设置字段的有效性 active=0 无效 逻辑删除
    */
    @PutMapping("/del/{id}")
    public CommonResult LoginDelById(@PathVariable int id) {
        User iUser = new User();
        iUser.setId(id);
        iUser.setActive(0);
        if (iUserService.updateById(iUser)){
            return CommonResult.successMsg("设置失效成功");
        }
        return CommonResult.failed("设置失效失败");
    }

    /**
     * 批量设置 active=0
    */
    @PutMapping("/batchDel")
    public CommonResult LoginBatchDel(@RequestParam String ids) {
        User iUser = new User();
        iUser.setActive(0);
        String[] idList = ids.split(",");
        for (String id : idList) {
            iUser.setId(Integer.parseInt(id));
            iUserService.updateById(iUser);
        }
        return CommonResult.successMsg("批量失效成功");
    }

    /**
     * 在数据表中真正的删除一条记录
    */
    @DeleteMapping("/{id}")
    public CommonResult delById(@PathVariable int id) {
        if (id<6) {
            return CommonResult.failed("不能删除系统的预留项");
        }
        logger.debug("正在删除id: "+id);
        if (iUserService.removeById(id)){
            return CommonResult.successMsg("删除成功");
        }
        return CommonResult.failed("删除失败");
    }

}
