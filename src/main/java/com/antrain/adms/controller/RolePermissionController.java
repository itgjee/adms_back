package com.antrain.adms.controller;

import java.util.Map;
import javax.annotation.Resource;

import com.antrain.adms.service.IRolePermissionService;
import com.antrain.adms.pojo.RolePermission;

import com.antrain.adms.utils.CommonResult;
import com.antrain.adms.utils.InitUtil;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.springframework.web.bind.annotation.*;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * 角色权限 前端控制器
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
*/
@RestController
@RequestMapping("/role-permissions")
public class RolePermissionController {
    private final Logger logger = LoggerFactory.getLogger(RolePermissionController.class);

    @Resource
    private IRolePermissionService iRolePermissionService;

    /**
    * 进行分页查询
    */
    @GetMapping
    public CommonResult getList(@RequestParam Map<String, Object> param) {
        InitUtil.initPage(param);
        int num = Integer.parseInt(param.get("page").toString());
        int limit = Integer.parseInt(param.get("limit").toString());
        QueryWrapper<RolePermission> wrapper = new QueryWrapper<>();
        InitUtil.initEq(param, wrapper, "active");
        IPage<RolePermission> page = new Page<>(num, limit);
        return CommonResult.success(iRolePermissionService.page(page, wrapper));
    }

    /**
     * 通过主键id在表中查找信息
    */
    @GetMapping("/{id}")
    public CommonResult getById(@PathVariable int id) {
        RolePermission iRolePermission = iRolePermissionService.getById(id);
        if (iRolePermission == null) {
            return CommonResult.failed();
        }
        return CommonResult.success(iRolePermission);
    }

    /**
     * 通过角色 id在表中查找信息
    */
    @GetMapping("/role/{id}")
    public CommonResult getByRoleId(@PathVariable int id) {
        QueryWrapper<RolePermission> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", id);
        RolePermission iRolePermission = iRolePermissionService.getOne(wrapper, false);
        if (iRolePermission == null) {
            return CommonResult.failed();
        }
        return CommonResult.success(iRolePermission);
    }

    /**
    * name : 需要检查那个字段的value是否在数据库存在
    * 比如/check/RegistworkName?value=阿司匹林  ：检查数据库是否存在RegistworkName='阿司匹林'
    */
    @GetMapping("/check/{name}")
    public CommonResult checkName(@PathVariable String name,@RequestParam String value){
        QueryWrapper<RolePermission> wrapper = new QueryWrapper<>();
        wrapper.eq(name, value);
        if (iRolePermissionService.getOne(wrapper) != null){
            return CommonResult.failed();
        }
        return CommonResult.success();
    }

    /**
     * 向表中添加一条记录
    */
    @PostMapping()
    public CommonResult save(@RequestBody RolePermission iRolePermission) {
        if (iRolePermissionService.save(iRolePermission)){
            return CommonResult.successMsg("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    /**
     * 根据表中的主键修改 根据表中字段相应的修改，如果存在则修改
    */
    @PutMapping("/{id}")
    public CommonResult update(@RequestBody RolePermission iRolePermission, @PathVariable int id) {
        iRolePermission.setId(id);
        if (iRolePermissionService.updateById(iRolePermission)){
            return CommonResult.successMsg("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

   

    
    /**
     * 在数据表中真正的删除一条记录
    */
    @DeleteMapping("/{id}")
    public CommonResult delById(@PathVariable int id) {
        if (id<6) {
            return CommonResult.failed("不能删除系统的预留项");
        }
        logger.debug("正在删除id: "+id);
        if (iRolePermissionService.removeById(id)){
            return CommonResult.successMsg("删除成功");
        }
        return CommonResult.failed("删除失败");
    }

}
