package com.antrain.adms.controller;

import java.util.Map;
import javax.annotation.Resource;

import com.antrain.adms.service.IUserRoleService;
import com.antrain.adms.pojo.UserRole;

import com.antrain.adms.utils.CommonResult;
import com.antrain.adms.utils.InitUtil;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.springframework.web.bind.annotation.*;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * 用户角色 前端控制器
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
*/
@RestController
@RequestMapping("/user-roles")
public class UserRoleController {
    private final Logger logger = LoggerFactory.getLogger(UserRoleController.class);

    @Resource
    private IUserRoleService iUserRoleService;

    /**
    * 进行分页查询
    */
    @GetMapping
    public CommonResult getList(@RequestParam Map<String, Object> param) {
        InitUtil.initPage(param);
        int num = Integer.parseInt(param.get("page").toString());
        int limit = Integer.parseInt(param.get("limit").toString());
        QueryWrapper<UserRole> wrapper = new QueryWrapper<>();
        InitUtil.initEq(param, wrapper, "active");
        IPage<UserRole> page = new Page<>(num, limit);
        return CommonResult.success(iUserRoleService.page(page, wrapper));
    }

    /**
     * 通过主键id在表中查找信息
    */
    @GetMapping("/{id}")
    public CommonResult getById(@PathVariable int id) {
        UserRole iUserRole = iUserRoleService.getById(id);
        if (iUserRole == null) {
            return CommonResult.failed();
        }
        return CommonResult.success(iUserRole);
    }

    /**
    * name : 需要检查那个字段的value是否在数据库存在
    * 比如/check/RegistworkName?value=阿司匹林  ：检查数据库是否存在RegistworkName='阿司匹林'
    */
    @GetMapping("/check/{name}")
    public CommonResult checkName(@PathVariable String name,@RequestParam String value){
        QueryWrapper<UserRole> wrapper = new QueryWrapper<>();
        wrapper.eq(name, value);
        if (iUserRoleService.getOne(wrapper) != null){
            return CommonResult.failed();
        }
        return CommonResult.success();
    }

    /**
     * 通过user_id在表中查找信息
    */
    @GetMapping("/user/{id}")
    public CommonResult getByUserId(@PathVariable int id) {
        QueryWrapper<UserRole> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", id);
        UserRole iUserRole = iUserRoleService.getOne(wrapper, false);
        if (iUserRole == null) {
            return CommonResult.failed();
        }
        return CommonResult.success(iUserRole);
    }   

    /**
     * 向表中添加一条记录
    */
    @PostMapping()
    public CommonResult save(@RequestBody UserRole iUserRole) {
        if (iUserRoleService.save(iUserRole)){
            return CommonResult.successMsg("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    /**
     * 根据表中的主键修改 根据表中字段相应的修改，如果存在则修改
    */
    @PutMapping("/{id}")
    public CommonResult update(@RequestBody UserRole iUserRole, @PathVariable int id) {
        iUserRole.setId(id);
        if (iUserRoleService.updateById(iUserRole)){
            return CommonResult.successMsg("修改成功");
        }
        return CommonResult.failed("修改失败");
    }


    /**
     * 在数据表中真正的删除一条记录
    */
    @DeleteMapping("/{id}")
    public CommonResult delById(@PathVariable int id) {
        if (id<6) {
            return CommonResult.failed("不能删除系统的预留项");
        }
        logger.debug("正在删除id: "+id);
        if (iUserRoleService.removeById(id)){
            return CommonResult.successMsg("删除成功");
        }
        return CommonResult.failed("删除失败");
    }

}
