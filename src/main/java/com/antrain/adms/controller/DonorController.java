package com.antrain.adms.controller;

import java.util.Map;

import javax.annotation.Resource;

import com.antrain.adms.pojo.Donor;
import com.antrain.adms.service.IDonorService;
import com.antrain.adms.utils.CommonResult;
import com.antrain.adms.utils.InitUtil;
import com.antrain.adms.utils.ShaUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 捐赠记录 前端控制器
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
*/
@RestController
@RequestMapping("/donors")
public class DonorController {
    private final Logger logger = LoggerFactory.getLogger(DonorController.class);

    @Resource
    private IDonorService iDonorService;

    /**
    * 进行分页查询
    */
    @GetMapping
    public CommonResult getList(@RequestParam Map<String, Object> param) {
        InitUtil.initPage(param);
        int num = Integer.parseInt(param.get("page").toString());
        int limit = Integer.parseInt(param.get("limit").toString());
        QueryWrapper<Donor> wrapper = new QueryWrapper<>();
        InitUtil.initEq(param, wrapper, "active");
        InitUtil.initLike(param, wrapper, "username");
        IPage<Donor> page = new Page<>(num, limit);
        return CommonResult.success(iDonorService.page(page, wrapper));
    }

    /**
     * 通过主键id在表中查找信息
    */
    @GetMapping("/{id}")
    public CommonResult getById(@PathVariable int id) {
        Donor iDonor = iDonorService.getById(id);
        if (iDonor == null) {
            return CommonResult.failed();
        }
        return CommonResult.success(iDonor);
    }

    /**
    * name : 需要检查那个字段的value是否在数据库存在
    * 比如/check/RegistworkName?value=阿司匹林  ：检查数据库是否存在RegistworkName='阿司匹林'
    */
    @GetMapping("/check/{name}")
    public CommonResult checkName(@PathVariable String name,@RequestParam String value){
        QueryWrapper<Donor> wrapper = new QueryWrapper<>();
        wrapper.eq(name, value);
        if (iDonorService.getOne(wrapper) != null){
            return CommonResult.failed();
        }
        return CommonResult.success();
    }

    /**
     * 向表中添加一条记录
    */
    @PostMapping()
    public CommonResult save(@RequestBody Donor iDonor) {
        if (StringUtils.isEmpty(iDonor.getPassword())){
            iDonor.setPassword(ShaUtil.getSHA256("123456"));
        } else {
            iDonor.setPassword(ShaUtil.getSHA256(iDonor.getPassword()));
        }
        if (iDonorService.save(iDonor)){
            return CommonResult.successMsg("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    /**
     * 根据表中的主键修改 根据表中字段相应的修改，如果存在则修改
    */
    @PutMapping("/{id}")
    public CommonResult update(@RequestBody Donor iDonor, @PathVariable int id) {
        iDonor.setId(id);
        if (!StringUtils.isEmpty(iDonor.getPassword())){
            iDonor.setPassword(ShaUtil.getSHA256(iDonor.getPassword()));
        }
        if (iDonorService.updateById(iDonor)){
            return CommonResult.successMsg("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

    /**
     * active 字段 相当于激活一样 设置字段的有效性 active=1 有效
    */
    @PutMapping("/back/{id}")
    public CommonResult update(@PathVariable int id) {
        Donor iDonor = new Donor();
        iDonor.setId(id);
        iDonor.setActive(1);
        if (iDonorService.updateById(iDonor)){
            return CommonResult.successMsg("激活成功");
        }
        return CommonResult.failed("激活失败");
    }

    /**
     * active 字段 相当于激活一样 设置字段的有效性 active=0 无效 逻辑删除
    */
    @PutMapping("/del/{id}")
    public CommonResult LoginDelById(@PathVariable int id) {
        Donor iDonor = new Donor();
        iDonor.setId(id);
        iDonor.setActive(0);
        if (iDonorService.updateById(iDonor)){
            return CommonResult.successMsg("设置失效成功");
        }
        return CommonResult.failed("设置失效失败");
    }

    /**
     * 批量设置 active=0
    */
    @PutMapping("/batchDel")
    public CommonResult LoginBatchDel(@RequestParam String ids) {
        Donor iDonor = new Donor();
        iDonor.setActive(0);
        String[] idList = ids.split(",");
        for (String id : idList) {
            iDonor.setId(Integer.parseInt(id));
            iDonorService.updateById(iDonor);
        }
        return CommonResult.successMsg("批量失效成功");
    }

    /**
     * 在数据表中真正的删除一条记录
    */
    @DeleteMapping("/{id}")
    public CommonResult delById(@PathVariable int id) {
        logger.debug("正在删除id: "+id);
        if (iDonorService.removeById(id)){
            return CommonResult.successMsg("删除成功");
        }
        return CommonResult.failed("删除失败");
    }

}
