package com.antrain.adms.mapper;

import com.antrain.adms.pojo.ProjectDonor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 项目捐赠 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface ProjectDonorMapper extends BaseMapper<ProjectDonor> {

}
