package com.antrain.adms.mapper;

import com.antrain.adms.pojo.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
