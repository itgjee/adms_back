package com.antrain.adms.mapper;

import com.antrain.adms.pojo.ConstantType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常数类别 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface ConstantTypeMapper extends BaseMapper<ConstantType> {

}
