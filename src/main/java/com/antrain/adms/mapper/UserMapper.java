package com.antrain.adms.mapper;


import com.antrain.adms.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface UserMapper extends BaseMapper<User> {

}
