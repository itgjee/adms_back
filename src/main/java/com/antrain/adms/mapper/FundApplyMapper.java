package com.antrain.adms.mapper;

import com.antrain.adms.pojo.FundApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 项目资金申请 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface FundApplyMapper extends BaseMapper<FundApply> {

}
