package com.antrain.adms.mapper;

import java.util.List;
import java.util.Map;

import com.antrain.adms.pojo.ConstantItem;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 常数项 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface ConstantItemMapper extends BaseMapper<ConstantItem> {
  List<Map<String, Object> > getListByTypeId(@Param(Constants.WRAPPER) Wrapper<ConstantItem> wrapper);
}
