package com.antrain.adms.mapper;


import com.antrain.adms.pojo.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface RoleMapper extends BaseMapper<Role> {
}
