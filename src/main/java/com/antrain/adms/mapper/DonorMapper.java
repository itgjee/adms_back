package com.antrain.adms.mapper;

import com.antrain.adms.pojo.Donor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 捐赠记录 Mapper 接口
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface DonorMapper extends BaseMapper<Donor> {

}
