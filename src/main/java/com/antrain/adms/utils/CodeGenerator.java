package com.antrain.adms.utils;

import java.util.ArrayList;
import java.util.List;

import com.antrain.adms.pojo.BasePojo;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

/**
 * 代码生成器修改数据库的连接信息 、父包名、不需要的表名前缀
 */
public class CodeGenerator {

    public static void main(String[] args) {
        // 定义临时需要用到的数据
        String url = "jdbc:mysql://localhost:3306/adms?useSSL=false&serverTimezone=Asia/Shanghai";
        String parentName ="com.antrain.adms";
        String tablePrefix = "sys";
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("antrain");
        gc.setOpen(false);
        // gc.setSwagger2(true); 实体属性 Swagger2 注解
        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(url);
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("654321");
        mpg.setDataSource(dsc);
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(parentName);
          // 包配置
        pc.setEntity("pojo");
        pc.setService("service");
        pc.setMapper("mapper");
        pc.setServiceImpl("service.impl");
        pc.setController("controller");
        mpg.setPackageInfo(pc);
        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName() + "/"
                        + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
      // 配置模板
      TemplateConfig templateConfig = new TemplateConfig();

      templateConfig.setController("controllerInit.java");

      mpg.setTemplate(templateConfig);

      // 策略配置
      StrategyConfig strategy = new StrategyConfig();
      // 数据库表映射到实体的命名策略
      strategy.setNaming(NamingStrategy.underline_to_camel);
      // 数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
      strategy.setColumnNaming(NamingStrategy.underline_to_camel);
      // 自定义继承的Entity类
      strategy.setSuperEntityClass(BasePojo.class);
      // 自定义基础的Entity类，公共字
      strategy.setSuperEntityColumns("id", "active", "update_time", "create_time");
      // 使用 lombok模型
      strategy.setEntityLombokModel(true);
      // 生成 @RestController 控制器
      strategy.setRestControllerStyle(true);
      // 驼峰转连字符
      strategy.setControllerMappingHyphenStyle(true);
      // 生成表名时去除前缀
      strategy.setTablePrefix(tablePrefix);
      // 生成实体时，生成字段注解
      strategy.setEntityTableFieldAnnotationEnable(true);
      mpg.setStrategy(strategy);
      mpg.setTemplateEngine(new FreemarkerTemplateEngine());
      mpg.execute();
    }
}