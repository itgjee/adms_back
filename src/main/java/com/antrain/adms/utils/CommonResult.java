package com.antrain.adms.utils;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonResult implements Serializable {
  private static final long serialVersionUID = 1245676543L;
  private Integer code;
    private String message;
    private Object obj;

    private CommonResult(Integer code, String message, Object obj) {
        this.code = code;
        this.message = message;
        this.obj = obj;
    }

    public static CommonResult success(Object obj)
    {
        return new CommonResult(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(),obj);
    }

    public static CommonResult success()
    {
        return new CommonResult(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(),null);
    }

    public static CommonResult successMsg(String msg)
    {
        return new CommonResult(ResultCode.SUCCESS.getCode(),msg,null);
    }

    public static CommonResult failed()
    {
        return new CommonResult(ResultCode.FAILED.getCode(), ResultCode.FAILED.getMessage(),null);
    }


    public static CommonResult failed(String msg){
        return new CommonResult(ResultCode.FAILED.getCode(),msg,null);
    }

    public static CommonResult failed(String msg,int code){
        return new CommonResult(code,msg,null);
    }
}
