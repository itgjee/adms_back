package com.antrain.adms.handler;

// import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.antrain.adms.exception.UserNoLoginException;
import com.antrain.adms.utils.JwtUtil;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @description ：拦截器去获取token并验证token
 */
public class AuthenticationInterceptor implements HandlerInterceptor {

  private final Logger logger = LoggerFactory.getLogger(AuthenticationInterceptor.class);

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,Object object) throws Exception {
    /** 地址过滤 */
    String uri = request.getRequestURI();
    logger.info("url="+uri);
    if (uri.endsWith("/login")||uri.endsWith("/error")) {
      return true;
    }
    // System.out.println(request);
    String token = request.getHeader("Authorization");// 从 http 请求头中取出 token
    // 如果不是映射到方法直接通过
    if (!(object instanceof HandlerMethod)) {
      return true;
    }
    // HandlerMethod handlerMethod = (HandlerMethod) object;
    // Method method = handlerMethod.getMethod();
    if (token == null) {
      throw new UserNoLoginException();
    }
    JwtUtil.isExpire(token);
    return true;
  }

  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object o,
      ModelAndView modelAndView) throws Exception {

  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse httpServletResponse, Object o,
      Exception e) throws Exception {
  }
}