package com.antrain.adms.service;

import com.antrain.adms.pojo.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IUserRoleService extends IService<UserRole> {

}
