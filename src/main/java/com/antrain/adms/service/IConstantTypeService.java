package com.antrain.adms.service;

import com.antrain.adms.pojo.ConstantType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常数类别 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IConstantTypeService extends IService<ConstantType> {

}
