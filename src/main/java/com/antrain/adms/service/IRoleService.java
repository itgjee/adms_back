package com.antrain.adms.service;

import com.antrain.adms.pojo.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IRoleService extends IService<Role> {

}
