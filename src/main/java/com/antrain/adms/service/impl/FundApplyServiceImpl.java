package com.antrain.adms.service.impl;

import com.antrain.adms.pojo.FundApply;
import com.antrain.adms.mapper.FundApplyMapper;
import com.antrain.adms.service.IFundApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目资金申请 服务实现类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Service
public class FundApplyServiceImpl extends ServiceImpl<FundApplyMapper, FundApply> implements IFundApplyService {

}
