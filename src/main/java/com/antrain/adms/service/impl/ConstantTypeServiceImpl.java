package com.antrain.adms.service.impl;

import com.antrain.adms.pojo.ConstantType;
import com.antrain.adms.mapper.ConstantTypeMapper;
import com.antrain.adms.service.IConstantTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常数类别 服务实现类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Service
public class ConstantTypeServiceImpl extends ServiceImpl<ConstantTypeMapper, ConstantType> implements IConstantTypeService {

}
