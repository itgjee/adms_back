package com.antrain.adms.service.impl;

import com.antrain.adms.pojo.Project;
import com.antrain.adms.mapper.ProjectMapper;
import com.antrain.adms.service.IProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目 服务实现类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements IProjectService {

}
