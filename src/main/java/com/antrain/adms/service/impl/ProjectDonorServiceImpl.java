package com.antrain.adms.service.impl;

import com.antrain.adms.pojo.ProjectDonor;
import com.antrain.adms.mapper.ProjectDonorMapper;
import com.antrain.adms.service.IProjectDonorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目捐赠 服务实现类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Service
public class ProjectDonorServiceImpl extends ServiceImpl<ProjectDonorMapper, ProjectDonor> implements IProjectDonorService {

}
