package com.antrain.adms.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.antrain.adms.mapper.PermissionMapper;
import com.antrain.adms.pojo.Permission;
import com.antrain.adms.pojo.RolePermission;
import com.antrain.adms.pojo.UserRole;
import com.antrain.adms.service.IPermissionService;
import com.antrain.adms.service.IRolePermissionService;
import com.antrain.adms.service.IUserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

  @Resource
  private IRolePermissionService roles;

  @Resource
  private IUserRoleService userRoles;

  @Override
  public Object listByTree() {
    QueryWrapper<Permission> wrapper = new QueryWrapper<>();
    wrapper.eq("parent_id", 0);
    List<Permission> list = this.list(wrapper);
    for (Permission i : list) {
      getChildren(i);
    }
    return list;
  }

  /**
   * 参数为父权限对象 通过父权限获取子权限, 这里假设为2级
   *
   */
  private void getChildren(Permission permission) {
    QueryWrapper<Permission> wrapper = new QueryWrapper<>();
    wrapper.eq("parent_id", permission.getId());
    // List<Permission> list =this.list(wrapper) ;
    // for (Permission i : list) {
    //   if (permission.getType() != 0) {
    //     getChildren(permission);
    //   }
    // }
    permission.setChildren(this.list(wrapper));
  }

  @Override
  public Object userPermissionList(int id) {
    //1根据用户id查询出所有的角色 user_role
    QueryWrapper<UserRole> userRoleQueryWrapper = new QueryWrapper<>();
    userRoleQueryWrapper.eq("user_id",id);
    UserRole userRole = userRoles.getOne(userRoleQueryWrapper,false);
    if(userRole==null || userRole.getRoleId() == null){
        return null;
    }

    // 根据角色获取权限id集合
    QueryWrapper<RolePermission> umsRolePermissionWrapper = new QueryWrapper<>();
    umsRolePermissionWrapper.inSql("role_id",userRole.getRoleId());
    List<RolePermission> rolePermissionList = roles.list(umsRolePermissionWrapper);
    if(rolePermissionList ==null ){
        return null;
    }

    // 去重
    Set<Integer> permissionIdSet = new HashSet<>();
    for (RolePermission i : rolePermissionList) {
        String [] pers = i.getPermissioinId().split(",");
        for (String j:pers){
          permissionIdSet.add(Integer.parseInt(j));
        }
    }

    //根据权限id查询权限树 现在获取2级
    QueryWrapper<Permission> permissionQueryWrapper = new QueryWrapper<>();
    permissionQueryWrapper.eq("parent_id",0).in("id",permissionIdSet);
    List<Permission> allPermission = this.list(permissionQueryWrapper);
    for (Permission i : allPermission) {
      permissionQueryWrapper.clear();
      permissionQueryWrapper.eq("parent_id",i.getId()).in("id", permissionIdSet);
      i.setChildren(this.list(permissionQueryWrapper));
    }
    return allPermission;
  }

}
