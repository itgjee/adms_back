package com.antrain.adms.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.antrain.adms.mapper.ConstantItemMapper;
import com.antrain.adms.pojo.ConstantItem;
import com.antrain.adms.service.IConstantItemService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 常数项 服务实现类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Service
public class ConstantItemServiceImpl extends ServiceImpl<ConstantItemMapper, ConstantItem>
    implements IConstantItemService {

  @Resource
  private ConstantItemMapper constantItemMapper;

  @Override
  public List<Map<String, Object>> getListByTypeId(int id) {
    QueryWrapper<ConstantItem> wrapper = new QueryWrapper<>();
    wrapper.eq("active", 1).eq("type_id",id);
    wrapper.orderByAsc("sort");
    return constantItemMapper.getListByTypeId(wrapper) ;
  }

}
