package com.antrain.adms.service.impl;

import com.antrain.adms.pojo.Donor;
import com.antrain.adms.mapper.DonorMapper;
import com.antrain.adms.service.IDonorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 捐赠记录 服务实现类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Service
public class DonorServiceImpl extends ServiceImpl<DonorMapper, Donor> implements IDonorService {

}
