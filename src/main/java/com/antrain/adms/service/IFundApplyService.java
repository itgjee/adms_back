package com.antrain.adms.service;

import com.antrain.adms.pojo.FundApply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目资金申请 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IFundApplyService extends IService<FundApply> {

}
