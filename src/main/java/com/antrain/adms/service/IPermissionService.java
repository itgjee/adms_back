package com.antrain.adms.service;

import com.antrain.adms.pojo.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IPermissionService extends IService<Permission> {
  Object listByTree();
  Object userPermissionList(int id);
}
