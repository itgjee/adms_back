package com.antrain.adms.service;

import com.antrain.adms.pojo.Donor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 捐赠记录 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IDonorService extends IService<Donor> {

}
