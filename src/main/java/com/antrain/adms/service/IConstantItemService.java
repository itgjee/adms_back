package com.antrain.adms.service;

import java.util.List;
import java.util.Map;

import com.antrain.adms.pojo.ConstantItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常数项 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IConstantItemService extends IService<ConstantItem> {
  List<Map<String, Object> > getListByTypeId(int id);
}
