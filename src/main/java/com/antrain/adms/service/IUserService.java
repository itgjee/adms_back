package com.antrain.adms.service;

import com.antrain.adms.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IUserService extends IService<User> {

}
