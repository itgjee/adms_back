package com.antrain.adms.service;

import com.antrain.adms.pojo.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限 服务类
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
public interface IRolePermissionService extends IService<RolePermission> {

}
