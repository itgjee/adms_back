package com.antrain.adms.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 捐赠记录
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("donor")
public class Donor extends BasePojo {


    /**
     * 用户名
     */
    @TableField("username")
    private String username;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 姓名
     */
    @TableField("realname")
    private String realname;

    /**
     * 性别（需要对应constant_item的id）男，女 未知 
     */
    @TableField("gender")
    private Integer gender;

    /**
     * 政治面貌（需要对应constant_item的id） 
     */
    @TableField("politic_outlook")
    private Integer politicOutlook;

    /**
     * 工作单位
     */
    @TableField("work_unit")
    private String workUnit;

    /**
     * 职务
     */
    @TableField("job")
    private String job;

    /**
     * 电话号码
     */
    @TableField("telephone")
    private String telephone;

    /**
     * 电子邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 公司股票代码
     */
    @TableField("stock_code")
    private String stockCode;

    /**
     * 备注
     */
    @TableField("remarks")
    private String remarks;


}
