package com.antrain.adms.pojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 项目资金申请
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("fund_apply")
public class FundApply extends BasePojo {

    /**
     * 对应项目申请的id
     */
    @TableField("project_id")
    private Integer projectId;

    /**
     * 资金申请名
     */
    @TableField("name")
    private String name;

    /**
     * 申请金额
     */
    @TableField("cost")
    private BigDecimal cost;

    /**
     * 资金申请详情
     */
    @TableField("detail")
    private String detail;

    /**
     * 资金申请状态 （需要对应constant_item的id）
     */
    @TableField("status")
    private Integer status;

    /**
     * 申请文件证明地址
     */
    @TableField("apply_file_url")
    private String applyFileUrl;


}
