package com.antrain.adms.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 常数项
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("constant_item")
public class ConstantItem extends BasePojo {
    /**
     * 类别id, 对应constant_type的主键id
     */
    @TableField("type_id")
    private Integer typeId;

    /**
     * 常数项代码
     */
    @TableField("code")
    private String code;

    /**
     * 常数项名称
     */
    @TableField("name")
    private String name;

    /**
     * 排序id，选择项显示的时候 从小到大排序
     */
    @TableField("sort")
    private Integer sort;


}
