package com.antrain.adms.pojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 项目捐赠
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("project_donor")
public class ProjectDonor extends BasePojo {

    /**
     * 对应项目申请的id
     */
    @TableField("project_id")
    private Integer projectId;

    /**
     * 捐赠名目
     */
    @TableField("name")
    private String name;

    /**
     * 捐赠人姓名
     */
    @TableField("donor_name")
    private String donorName;

    /**
     * 捐赠金额
     */
    @TableField("money")
    private BigDecimal money;

    /**
     * 捐赠详情
     */
    @TableField("detail")
    private String detail;

    /**
     * 项目捐赠状态 （需要对应constant_item的id）
     */
    @TableField("status")
    private Integer status;

    /**
     * 协议文件地址
     */
    @TableField("treaty_file_url")
    private String treatyFileUrl;

    /**
     * 捐赠方审批文件地址
     */
    @TableField("donor_approve_url")
    private String donorApproveUrl;

    /**
     * 基金会审批文件地址
     */
    @TableField("fund_approve_url")
    private String fundApproveUrl;


}
