package com.antrain.adms.pojo;

import java.time.LocalDateTime;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasePojo {
	// 主键
    @TableId(type = IdType.AUTO)
    private Integer id;

 	// 分页的页码 根据自己是否需要
    @JSONField(serialize = false)
    @TableField(exist = false)
    private Integer page =1;

	// 每页的大小 根据自己是否需要
    @JSONField(serialize = false)
    @TableField(exist = false)
    private Integer limit=5;

	// 是否分页 1 分页 0 不分页 根据自己是否需要
    @JSONField(serialize = false)
    @TableField(exist = false)
    private Integer withPage =1;
    
    @TableField("active")
    private Integer active;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;
}
