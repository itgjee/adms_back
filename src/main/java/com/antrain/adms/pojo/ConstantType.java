package com.antrain.adms.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 常数类别
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("constant_type")
public class ConstantType extends BasePojo {


    /**
     * 代码
     */
    @TableField("code")
    private String code;

    /**
     * 中文名称
     */
    @TableField("name")
    private String name;


}
