package com.antrain.adms.pojo;

import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 权限
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_permission")
public class Permission extends BasePojo {

    /**
     * 权限名称
     */
    @TableField("name")
    private String name;

    /**
     * 资源导航路径
     */
    @TableField("url")
    private String url;

    /**
     * 菜单显示时的图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 权限类型（需要对应constant_item的id） 目录  导航
     */
    @TableField("type")
    private Integer type;

    /**
     * 上级权限的id，默认根id=0,即无父权限
     */
    @TableField("parent_id")
    private Integer parentId;

    @TableField(exist = false)
    private List<Permission> children;
}
