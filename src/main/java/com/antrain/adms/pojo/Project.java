package com.antrain.adms.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 项目
 * </p>
 *
 * @author antrain
 * @since 2020-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("project")
public class Project extends BasePojo {

    /**
     * 项目名
     */
    @TableField("name")
    private String name;

    /**
     * 项目负责人姓名
     */
    @TableField("leader_name")
    private String leaderName;

    /**
     * 项目详情
     */
    @TableField("detail")
    private String detail;

    /**
     * 项目状态 （需要对应constant_item的id）
     */
    @TableField("status")
    private Integer status;

    /**
     * 申请文件地址
     */
    @TableField("apply_file_url")
    private String applyFileUrl;

    /**
     * 审批文件地址
     */
    @TableField("approve_file_url")
    private String approveFileUrl;

    /**
     * 项目结题文件地址
     */
    @TableField("conclusion_file_url")
    private String conclusionFileUrl;


}
